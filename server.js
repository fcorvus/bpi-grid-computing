const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
const port = process.env.PORT || 3000;

// Getting our api routes
const api = require('./server/routes/api');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// Using middleware
app.use(express.static(path.join(__dirname, 'dist/grid-computing')));

app.use('/api', api);

// Catch all other routes request and return it to the index
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/grid-computing/index.html'));
});

const server = http.createServer(app);


server.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
