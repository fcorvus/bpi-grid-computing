const express = require('express');
const router = express.Router();

const mysql = require('mysql2');
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'grid',
  password: ''
});
const Nightmare = require('nightmare');




// Get post
router.get('/users', (req, res) => {
  console.log('request', req);
  db.query('SELECT * FROM users', (err, result) => {
    if(err){
        res.send(JSON.stringify({"status": 500, "error": err, "data": null}));
    } else{
        res.send(JSON.stringify({"status": 200, "error": null, "data": result}));
    }
  });
});

router.post('/examine-url', (req, res) => {
  const nightmare = Nightmare({ show: false });
  let examineURL = req.body.url;
  let username = req.body.username;
  console.log('url -> ' + username +': '+ examineURL);
  nightmare.goto(req.body.url)
  .wait(2000)
  .evaluate(() => {
    const anchors = document.getElementsByTagName('a');
    const hrefs = [];
    for (let i = 0; i < anchors.length; i++) {
      if (anchors[i].href !== '') {
        hrefs.push(anchors[i].href);
      }
    }
    return hrefs;
  })
  .end()
  .then((hrefs) => {
    res.send(JSON.stringify({data: hrefs}));

    for (const url of hrefs) {
      var recurso = url.slice(-8);
      if (/.+(\.((doc)|(DOC)|(mp3)|(MP3)|(pdf)|(PDF)|(jpg)|(JPG)|(ppt)|(PPT)|(zip)|(ZIP)|(rar)|(RAR)|(java)|(JAVA)|(mp4)|(MP4)|(PNG)|(png))$)/.test(recurso)) {
        console.log('RECURSO: ', url);
        db.query(`INSERT INTO recursos VALUES (0,'${examineURL}','${url}')`, (err, result) => {
          if(err){
              // console.log('not inserted');
          } else{
              // console.log('inserted');
          }
        });
      } else {
        db.query(`INSERT INTO urls VALUES ('${url}',0)`, (err, result) => {
          if(err){
              // console.log('not inserted');
          } else{
              // console.log('inserted');
          }
        });
      }
    }
  })

});


module.exports = router;
