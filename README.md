- Hacer una página web donde cada cliente debe de ingresar al sistema (login y password).
- Se capturan todos los datos posibles del cliente y se envían al servidor para su almacenamiento.
- El servidor le envía al cliente (vía AJAX) la dirección de una página Web.
- El cliente debe de descargar esa página y encontrar todos los recursos que se encuentren en dicha página (documentos, videos, sonidos, textos, enlaces, etc.).
- Cada recurso que encuentre el cliente deberá ser enviado al servidor para que lo indexe y almacene en una base de datos
- Si se encuentra un enlace debe almacenarse y marcarse como pendiente para ser revisado posteriormente.
- El proceso se repite constantemente mientras haya clientes disponibles y enlaces disponibles para revisar.

# GridComputing

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
