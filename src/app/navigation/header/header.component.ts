import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Output() sidenavToggle = new EventEmitter<void>();
  isAuth = false;
  authSubscription: Subscription;
  username;
  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authSubscription = this.authService.authChange.subscribe(
      authStatus => {
        this.isAuth = authStatus;
        this.username = localStorage.getItem('username');
      }
    );
  }
  onToggleSidenav() {
    this.sidenavToggle.emit();
  }
  logout() {
    this.authService.logout();
    this.authService.updateAuthChange(false);
    this.router.navigate(['/login']);
  }
  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }
}
