import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/welcome']);
    }
  }
  onSubmit(form: NgForm) {
    this.authService.login(form.value.email, form.value.password).subscribe(
      res => {
        localStorage.setItem('username', res['data'][0].username);
        localStorage.setItem('user_id', res['data'][0].user_id);
        this.router.navigate(['/welcome']);
      },
      err => {
        alert('Contraseña y/o Email no validos');
      }
    );
  }

}
