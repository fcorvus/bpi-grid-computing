import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/welcome']);
    }
  }
  onSubmit(form: NgForm) {
    this.userService.saveUser(form.value.username, form.value.email, form.value.password).subscribe(
      res => {
        if (res['insert_status']) {
          localStorage.setItem('username', form.value.username);
          localStorage.setItem('user_id', res['user_id']);
          this.router.navigate(['/welcome']);
        } else {
          alert('El email/usuario ya se encuentra registrado');
        }
      },
      err => {
        alert('Ocurrio un error, por favor intente de nuevo');
      }
    );
  }
}
