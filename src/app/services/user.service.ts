import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Domain } from '../shared/domain';
const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  domain = Domain;
  uploader;
  uploaderStatus = false;
  constructor(
    private httpClient: HttpClient
  ) {
   }
  saveUser(username: string, email: string, password: string) {
    return this.httpClient.post(this.domain, {username: username, email: email, password: password, operation: 'save'});
  }
  saveUserInformation(country, isp, bn, bv, bl, bp, user_id) {
    const body = JSON.stringify({
      country: country,
      isp: isp,
      browserName: bn,
      browserVersion: bv,
      browserLanguage: bl,
      browserPlatform: bp,
      userId: user_id,
      operation: 'save_user_information'
    });
    return this.httpClient.post(this.domain, body, httpOptions);
  }
  examineUrl(url: string, username: any) {
    return this.httpClient.post(`http://192.168.100.2:3000/api/examine-url`, {url: url, username: username});
  }
  getUrl() {
    return this.httpClient.post(this.domain, {operation: 'get_url'});
  }
}
