import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Subject } from 'rxjs';
import { Domain } from '../shared/domain';

const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  domain = Domain;
  authChange = new Subject<boolean>();
  userId;

  constructor(
    private httpClient: HttpClient
  ) { }
  login(email: string, password: string) {
    const body = JSON.stringify({email: email, password: password, operation: 'login'});
    return this.httpClient.post(this.domain, body, httpOptions);
  }
  isLoggedIn() {
    if (!localStorage.getItem('username')) {
      this.authChange.next(false);
      return false;
    } else {
      this.authChange.next(true);
      return true;
    }
  }
  updateAuthChange(status: boolean) {
    this.authChange.next(status);
  }
  logout() {
    localStorage.removeItem('username');
    localStorage.removeItem('user_id');
  }
  getIpApi() {
    return this.httpClient.get('https://json.geoiplookup.io');
  }
}
