import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(
    private _auth: AuthService,
    private _user: UserService
  ) {}

  ngOnInit() {
    this._auth.getIpApi().subscribe(
      (res: IP) => {
        console.log(res);
        this._user.saveUserInformation(
          res.country_name,
          res.isp,
          navigator.appName,
          navigator.appVersion,
          navigator.language,
          navigator.platform,
          localStorage.getItem('user_id')
          ).subscribe(
            result => {
              console.log('SAVE INFORMATION: ', result);
              this.onClick();
            }, error => {
              console.log(error);
            }
          );
      },
      error => {
        console.log(error);
        this._user.saveUserInformation(
          '',
          '',
          navigator.appName,
          navigator.appVersion,
          navigator.language,
          navigator.platform,
          localStorage.getItem('user_id')
          ).subscribe(
            result => {
              console.log('SAVE INFORMATION: ', result);
              this.onClick();
            }, err => {
              console.log(err);
            }
          );

      }
    );
  }
  onClick() {
    this._user.getUrl().subscribe(
      res => {
        console.log('URL: ', res);
        this._user.examineUrl(res['data'], localStorage.getItem('username')).subscribe(
          result => {
            console.log(result);
            this.onClick();
          }, error => {
            console.log(error);
            this.onClick();
          }
        );
      }, err => {
        console.log(err);
        // this.onClick();
      }
    );
  }

}
export interface IP {
  country_name: string;
  isp: string;
}
